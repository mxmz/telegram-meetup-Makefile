

LOCAL_GOPATH=$(shell readlink -f ..)/_local/go
ROOT_PATH=$(shell readlink -f ..)

GOPATH:=$(LOCAL_GOPATH):$(GOPATH)

GODEPS=google.golang.org/appengine 		\
	   github.com/cnf/structhash 		\
	   github.com/onsi/gomega 			\
	   gopkg.in/telegram-bot-api.v4		\
	   github.com/glycerine/zebrapack

env:
	env | egrep ^GO

test: deps
	cd $(ROOT_PATH) && go test ./...

deps:
	mkdir -p $(LOCAL_GOPATH)/src
	ln -sf $(ROOT_PATH) $(LOCAL_GOPATH)/src/
	go get $(GODEPS)

updatedeps: deps
	go get -u -x $(GODEPS)

generate:
	cd ../types && zebrapack -file dto.go -msgp

gaeserve: 
	cd $(ROOT_PATH)/gaeapp && goapp serve 

gaedeployv1: 
	cd $(ROOT_PATH)/gaeapp && goapp deploy --version v1 
	curl https://meetup-chatbot.appspot.com/init

clean:
	rm -vrf $(LOCAL_GOPATH)
